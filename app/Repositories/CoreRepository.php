<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CoreRepository
 * @package App\Repositories
 */
abstract class CoreRepository
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|Model|mixed
     */
    protected function startConditions()
    {
        return clone $this->model;
    }

    /**
     * @var \Illuminate\Contracts\Foundation\Application|Model|mixed
     */
    protected $model;

    /**
     * @return mixed
     */
    abstract protected function getModelClass();

    /**
     * CoreRepository constructor.
     */
    public function __construct()
    {
        $this->model = app($this->getModelClass());
    }

    /**
     * data from admin request for table (Vue Good Table)
     *
     * @param array $data
     * @return array
     */
    protected function getVariablesForTables(array $data)
    {
        $collected_data = collect($data);

        $select = $collected_data->get('select');
        $search = $collected_data->get('search');
        $value = explode( ',', $collected_data->get('value') );

        return compact('select', 'search', 'value');
    }

}
