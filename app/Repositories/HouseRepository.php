<?php
namespace App\Repositories;

use App\Models\House;


class HouseRepository extends CoreRepository
{

    public function getSentences($request)
    {
        $vars = $this->getVariablesForTables($request);
        $query = $this->startConditions();
        $max_price = $query->max('price');
        $test = 'no';

        // search
        if($vars['select'] != '' && $vars['search'] != ''){
            if($vars['select'] == 'name'){
                $query = $query->where('name', 'like', $vars['search'] . '%')
                    ->orWhere('name', 'like', '%' . $vars['search'] . '%')
                    ->orWhere('name', 'like', '%' . $vars['search']);
            }
            else if($vars['select'] == 'bedroom'){
                $query = $query->where('bedroom', 'like', $vars['search']);
            }
            else if($vars['select'] == 'bathroom'){
                $query = $query->where('bathroom', 'like', $vars['search']);
            }
            else if($vars['select'] == 'garage'){
                $query = $query->where('garage', 'like', $vars['search']);
            }
            else if($vars['select'] == 'storey'){
                $query = $query->where('storey', 'like', $vars['search']);
            }
        }

        if( is_array($vars['value']) && count($vars['value']) == 2 && is_int((int)$vars['value'][0]) && is_int((int)$vars['value'][1])){
            $query = $query->where('price', '>=', $vars['value'][0])
                ->where('price', '<=', $vars['value'][1]);
        }

        $list = $query
            ->get();


        $value = $vars['value'];

        return compact( 'list','max_price', 'test', 'value');
    }





    /**
     * @inheritDoc
     */
    protected function getModelClass()
    {
        return House::class;
    }
}
