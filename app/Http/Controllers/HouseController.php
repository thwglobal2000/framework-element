<?php
namespace App\Http\Controllers;

use App\Http\Requests\GetRequest;
use App\Http\Responses\ApiResponse;
use App\Repositories\HouseRepository;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    protected $houseRepository;

    public function __construct() {
        $this->houseRepository = new HouseRepository();
    }

    public function index(GetRequest $request) {
        $houses = $this->houseRepository->getSentences($request->validated());

        return new ApiResponse(compact('houses'));
    }
}
