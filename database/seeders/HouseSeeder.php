<?php
namespace Database\Seeders;


use App\Models\House;
use Illuminate\Database\Seeder;

class HouseSeeder extends Seeder {

    private $data = [
        [ 'name' => 'The Victoria', 'price' => 374662, 'bedroom' => 4, 'bathroom' => 2, 'storey' => 2, 'garage' => 2 ],
        [ 'name' => 'The Xavier', 'price' => 513268, 'bedroom' => 4, 'bathroom' => 2, 'storey' => 1, 'garage' => 2 ],
        [ 'name' => 'The Como', 'price' => 454990, 'bedroom' => 4, 'bathroom' => 3, 'storey' => 2, 'garage' => 3 ],
        [ 'name' => 'The Aspen', 'price' => 384356, 'bedroom' => 4, 'bathroom' => 2, 'storey' => 2, 'garage' => 2 ],
        [ 'name' => 'The Lucretia', 'price' => 572002, 'bedroom' => 4, 'bathroom' => 3, 'storey' => 2, 'garage' => 2 ],
        [ 'name' => 'The Toorak', 'price' => 521951, 'bedroom' => 5, 'bathroom' => 2, 'storey' => 1, 'garage' => 2 ],
        [ 'name' => 'The Skyscape', 'price' => 263604, 'bedroom' => 3, 'bathroom' => 2, 'storey' => 2, 'garage' => 2 ],
        [ 'name' => 'The Clifton', 'price' => 386103, 'bedroom' => 3, 'bathroom' => 2, 'storey' => 1, 'garage' => 1 ],
        [ 'name' => 'The Geneva', 'price' => 390600, 'bedroom' => 4, 'bathroom' => 3, 'storey' => 2, 'garage' => 2 ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        House::insert($this->data);
    }
}
