require('./bootstrap');
window.Vue = require('vue').default;

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)


Vue.component('element-component', require('./components/ElementComponent.vue').default);

const app = new Vue({
    el: '#app',
});
